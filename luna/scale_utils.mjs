//----------------------------------------------------------------------------//
//                                         888                                //
//                                         888                                //
//                                         888                                //
//                         8888b.  888d888 888  888                           //
//                            `88b 888P'   888 .88P                           //
//                        .d888888 888     888888K                            //
//                        888  888 888     888 `88b                           //
//                        `Y888888 888     888  888                           //
//                                                                            //
//  File      : scale_utils.mjs                                               //
//  Project   : doom_fire                                                     //
//  Date      : 13 Dec, 21                                                    //
//  License   : GPLv3                                                         //
//  Author    : stdmatt <stdmatt@pixelwizards.io>                             //
//  Copyright : stdmatt - 2021                                                //
//                                                                            //
//  Description :                                                             //
//  Based upon the work of:                                                   //
//      https://github.com/massanchik/adaptive-scale-js                       //
//---------------------------------------------------------------------------~//

//----------------------------------------------------------------------------//
// Imports                                                                    //
//----------------------------------------------------------------------------//
//------------------------------------------------------------------------------
import { Math_Utils } from "./math_utils.mjs";


//----------------------------------------------------------------------------//
// Scale_Utils                                                                //
//----------------------------------------------------------------------------//
//------------------------------------------------------------------------------
export class Scale_Utils
{
    //------------------------------------------------------------------------//
    // Constants                                                              //
    //------------------------------------------------------------------------//
    //--------------------------------------------------------------------------
    static POLICY_NONE         = 0;
    static POLICY_EXACT_FIT    = 1;
    static POLICY_NO_BORDER    = 2;
    static POLICY_FULL_HEIGHT  = 3;
    static POLICY_FULL_WIDTH   = 4;
    static POLICY_SHOW_ALL     = 5;


    //------------------------------------------------------------------------//
    // Public Functions                                                       //
    //------------------------------------------------------------------------//
    //--------------------------------------------------------------------------
    static get_scaled_rect(container, target, policy)
    {
        if(policy == Scale_Utils.POLICY_NONE) {
            return target;
        }

        const aspect_ratio = container.width / container.height;
        let scale_aspect = Math_Utils.make_size(
            container.width  / target.width,
            container.height / target.height
        );

        let is_calculated = false;

        let final_rect = Math_Utils.make_rect(0, 0, 0, 0);
        let new_target = Math_Utils.make_size(target.width, target.height);

        switch (policy) {
            case Scale_Utils.POLICY_EXACT_FIT: {
                // Empty...
            } break;

            case Scale_Utils.POLICY_NO_BORDER: {
                scale_aspect.width = scale_aspect.height = Math.max(scale_aspect.width, scale_aspect.height);
            } break;

            case Scale_Utils.POLICY_SHOW_ALL: {
                scale_aspect.width = scale_aspect.height = Math.min(scale_aspect.width, scale_aspect.height);
            } break;

            case Scale_Utils.POLICY_FULL_WIDTH: {
                scale_aspect.height = scale_aspect.width;
                new_target.width    = Math.ceil(container.width / scale_aspect.width);
            } break;

            case Scale_Utils.POLICY_FULL_HEIGHT: {
                scale_aspect.width = scale_aspect.height;
                new_target.height  = Math.ceil(container.height / scale_aspect.height);
            } break;

            default: {
                final_rect.x      = final_rect.y = 0;
                final_rect.width  = target.width;
                final_rect.height = target.height;
                is_calculated     = true;
            }
        }

        if (!is_calculated) {
            const width  = (new_target.width  * scale_aspect.width );
            const height = (new_target.height * scale_aspect.height);
            final_rect = Math_Utils.make_rect(
                (container.width  - width ) / 2,
                (container.height - height) / 2,
                width,
                height,
            );
        }

        return final_rect;
    };
}