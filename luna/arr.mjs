export class Arr
{
    //--------------------------------------------------------------------------
    static create(size, default_value = null)
    {
        let arr = new Array(size);
        arr.fill(default_value);
        return arr;
    }

    //--------------------------------------------------------------------------
    static create_2d(h, w, default_value = null)
    {
        let arr = [];
        for(let i = 0; i < h; ++i) {
            arr.push([]);
            for(let j = 0; j < w; ++j) {
                arr[i].push(default_value);
            }
        }
        return arr;
    }

    //--------------------------------------------------------------------------
    static is_empty(arr)
    {
        return arr.length == 0;
    }

    //--------------------------------------------------------------------------
    static index_of(arr, func)
    {
        // @TODO(stdmatt): use js stuff...
        for(let i = 0; i < arr.length; ++i) {
            if(func(arr[i])) {
                return i;
            }
        }
        return -1;
    }

    //--------------------------------------------------------------------------
    static find_if(arr, func)
    {
        let r = arr.find(func);
        return r;
    }

    //--------------------------------------------------------------------------
    static contains(arr, func)
    {
        let r = arr.find(func);
        if(r == undefined) {
            return false;
        }
        return true;
    }

    //--------------------------------------------------------------------------
    static remove_front(arr)
    {
        arr = arr.splice(0, 1);
    }

    //--------------------------------------------------------------------------
    static remove_back(arr)
    {
        arr = arr.splice(arr.length-1, 1);
    }

    //--------------------------------------------------------------------------
    static remove_at(arr, i)
    {
        arr = arr.splice(i, 1);
    }

    //--------------------------------------------------------------------------
    static remove_if(arr, pred)
    {
        for(let i = 0; i < arr.length; ++i) {
            if(pred(arr[i])) {
                Arr.remove_at(arr, i);
                return;
            }
        }
    }


    //--------------------------------------------------------------------------
    static push_front(arr, e)
    {
        arr.unshift(e);
    }

    //--------------------------------------------------------------------------
    static push_back(arr, e)
    {
        arr.push(e);
    }

    //--------------------------------------------------------------------------
    static pop_back(arr)
    {
        let e = Arr.get_back(arr);
        arr = arr.splice(arr.length -1, 1);
        return e;
    }

    //--------------------------------------------------------------------------
    static pop_front(arr)
    {
        let e = Arr.get_front(arr);
        arr = arr.splice(0, 1);
        return e;
    }
    //--------------------------------------------------------------------------
    static get(arr, i)
    {
        if(i >= arr.length) {
            debugger;
        }

        if(i < 0) {
            i = (arr.length + i);
        }

        return arr[i];
    }

    //--------------------------------------------------------------------------
    static get_back(arr)
    {
        if(Arr.is_empty(arr)) {
            return null;
        }
        return arr[arr.length-1];
    }

    //--------------------------------------------------------------------------
    static get_front(arr)
    {
        if(Arr.is_empty(arr)) {
            return null;
        }
        return arr[0];
    }

    //--------------------------------------------------------------------------
    static random_element(arr, rnd_func)
    {
        if(arr.length == 0) {
            return null;
        }

        const index = Math.trunc((Math.random() * arr.length));
        return arr[index];
    }
};
