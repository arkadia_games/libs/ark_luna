import { RES } from "./res.mjs";
import * as UI from "./ui.mjs";


// @XXX(stdmatt): Layout has a very nasty behaviour of setting the objects
// anchor point to center, or requiring it...
// Now it's done cause we can assume that our needs are being met, but
// eventually we need to refactor it...
//



//
// Layout Utils
//
export class Layout
{
    static DESIGN_WIDTH  = 0;
    static DESIGN_HEIGHT = 0;

    //
    // Position
    //
    //              Top
    //        +------------+
    //        |            |
    //  Left  |   Center   |  Right
    //        |            |
    //        +------------+
    //             Bottom

    //--------------------------------------------------------------------------
    static get_top_of(obj)
    {
        const anchor = Layout.get_anchor_from(obj);
        const offset = (obj.height * anchor.y);
        return obj.y - offset;
    }

    //--------------------------------------------------------------------------
    static set_top_of(y, obj)
    {
        const anchor = Layout.get_anchor_from(obj);
        const offset = (obj.height * anchor.y);
        obj.y = y + offset;
    }

    //--------------------------------------------------------------------------
    static get_bottom_of(obj)
    {
        const anchor = Layout.get_anchor_from(obj);
        // Make anchor point from 0,1 to 1,0
        const nay    = (1 - anchor.y);
        const offset = (obj.height * nay);

        return obj.y + offset;
    }

    //--------------------------------------------------------------------------
    static set_bottom_of(y, obj)
    {
        const anchor = Layout.get_anchor_from(obj);
        // Make anchor point from 0,1 to 1,0
        const nay    = (1 - anchor.y);
        const offset = (obj.height * nay);
        obj.y = y + offset;
    }

    //--------------------------------------------------------------------------
    // x - is where the center of the given object will be placed.
    static set_x_relative_to_design_size(x, ...objs)
    {
        for(let i = 0; i < objs.length; ++i) {
            const obj          = objs[i];
            const anchor_point = Layout.get_anchor_from(obj);

            // Make anchor point from 0,1 to -1,+1.
            const nax      = -1 + anchor_point.x * 2;
            const target_x = Layout.DESIGN_WIDTH * x + (obj.width  * 0.5 * nax);

            obj.x = Layout.DESIGN_WIDTH * x
        }
    }

    //--------------------------------------------------------------------------
    // y - is where the center of the given object will be placed.
    static set_y_relative_to_design_size(y, ...objs)
    {
        for(let i = 0; i < objs.length; ++i) {
            const obj          = objs[i];
            const anchor_point = Layout.get_anchor_from(obj);

            // Make anchor point from 0,1 to -1,+1.
            const nay      = -1 + anchor_point.y * 2;
            const target_y = Layout.DESIGN_HEIGHT * y + (obj.height * 0.5 * nay);

            obj.y = Layout.DESIGN_HEIGHT * y;
        }
    }

    //--------------------------------------------------------------------------
    // x - is where the center of the given object will be placed.
    // y - is where the center of the given object will be placed.
    static set_center_relative_to_design_size(x, y, ...objs)
    {
        for(let i = 0; i < objs.length; ++i) {
            const obj          = objs[i];
            const anchor_point = Layout.get_anchor_from(obj);

            // Make anchor point from 0,1 to -1,+1.
            const nax  = -1 + anchor_point.x * 2;
            const nay  = -1 + anchor_point.y * 2;

            const target_x = Layout.DESIGN_WIDTH  * x + (obj.width  * 0.5 * nax);
            const target_y = Layout.DESIGN_HEIGHT * y + (obj.height * 0.5 * nay);

            obj.x = target_x;
            obj.y = target_y;
        }
    }


    //
    // Stack
    //
    //--------------------------------------------------------------------------
    static stack_objects_vertically(gap, ref_obj, ...objs)
    {
        let prev_obj = ref_obj;
        for(let i = 0; i < objs.length; ++i) {
            const prev_bottom = Layout.get_bottom_of(prev_obj);

            const curr_obj = objs[i];
            Layout.set_top_of(prev_bottom + gap, curr_obj);

            prev_obj = curr_obj;
        }
    }



    //
    // Downstairs is not bullet proofed...
    //











    static create_container(...children)
    {
        const c = new PIXI.Container();
        if(children.length != 0) {
            c.addChild(...children);
        }
        return c;
    }


    static create_fixed_size_container(width, height, ...children)
    {
        const c = new UI.Fixed_Size_Container(width, height);
        if(children.length != 0) {
            c.addChild(...children);
        }
        return c;
    }

    //
    // Anchor
    //
    //------------------------------------------------------------------------------
    static set_anchor_to(x, y, obj)
    {
        if(y == null || y == undefined) {
            y = x;
        }
        if(obj.anchor) {
            obj.anchor.set(x, y);
        } else {
            obj.pivot.set(
                x * obj.width  / obj.scale.x,
                y * obj.height / obj.scale.y
            )
        }
    }

    //--------------------------------------------------------------------------
    static get_anchor_from(obj)
    {
        if(obj.anchor) {
            return obj.anchor;
        } else {
            const size = new PIXI.Point(
                obj.width  / obj.scale.x,
                obj.height / obj.scale.y
            );
            if(size.x == 0 || size.y == 0) {
                return size;
            }

            return new PIXI.Point(
                obj.pivot.x / size.x,
                obj.pivot.y / size.y
            );
        }
    }

    //--------------------------------------------------------------------------
    static set_anchor_to_center(...items)
    {
        for(let i = 0; i < items.length; ++i) {
            Layout.set_anchor_to(0.5, 0.5, items[i]);
        }
    }

    //--------------------------------------------------------------------------
    static add_to_parent(parent, ...objs)
    {
        for(let i = 0; i < objs.length; ++i) {
            parent.addChild(objs[i]);
        }
    }

    //--------------------------------------------------------------------------
    static remove_from_parent(...objs)
    {
        for(let i = 0; i < objs.length; ++i) {
            if(objs[i] && objs[i].parent) {
                objs[i].parent.removeChild(objs[i]);
            }
        }
    }


    //--------------------------------------------------------------------------
    static get_left_of(obj)
    {
        const anchor = Layout.get_anchor_from(obj);
        const offset = (obj.width * anchor.x);
        return obj.x - offset;
    }


    //--------------------------------------------------------------------------
    static set_left_of(x, obj)
    {
        const anchor = Layout.get_anchor_from(obj);
        const offset = (obj.width * anchor.x);
        obj.x = x + offset;
    }

    //--------------------------------------------------------------------------
    static get_center_of(obj)
    {
        const anchor = Layout.get_anchor_from(obj);
        return new PIXI.Point(
            obj.x + (obj.width  * anchor.x) + (obj.width  * 0.5),
            obj.y + (obj.height * anchor.y) + (obj.height * 0.5)
        );
    }

    //--------------------------------------------------------------------------
    static set_center_of(x, y, ...objs)
    {
        for(let i = 0; i < objs.length; ++i) {
            const obj = objs[i];
            const w = obj.width;
            const h = obj.height;

            Layout.set_top_of (y - h * 0.5, obj);
            Layout.set_left_of(x - w * 0.5, obj);
        }
    }


    //--------------------------------------------------------------------------
    static set_center_relative_to_parent(...objs)
    {
        Layout.set_anchor_to_center(...objs); // @XXX(stdmatt): remove the anchor modification
        for(let i = 0; i < objs.length; ++i) {
            const parent = objs[i].parent;
            const cx     = parent.width  * 0.5;
            const cy     = parent.height * 0.5;
            objs[i].x = cx;
            objs[i].y = cy;
        }
    }

    //--------------------------------------------------------------------------
    static set_x_relative_to_parent(x, ...objs)
    {
        const parent_width = objs[0].parent.width;
        for(let i = 0; i < objs.length; ++i) {
            const curr_obj     = objs[i];
            const curr_anchor  = Layout.get_anchor_from(curr_obj);
            curr_obj.x = (parent_width * x) - (curr_obj.width * 0.5) - (curr_anchor.x * curr_obj.width);
        }
    }

    //--------------------------------------------------------------------------
    static set_y_relative_to_parent(y, ...objs)
    {
        const parent_height = objs[0].parent.height;
        for(let i = 0; i < objs.length; ++i) {
            const curr_obj     = objs[i];
            const curr_anchor  = Layout.get_anchor_from(curr_obj);
            curr_obj.y = (parent_height * y) - (curr_obj.height* 0.5) + (curr_anchor.y * curr_obj.height);
        }
    }

    //
    // Stack
    //
    static stack_positions_v(gap, ...objs)
    {
        for(let i = 1; i < objs.length; ++i) {
            const prev = objs[i-1];
            const prev_anchor      = Layout.get_anchor_from(prev);
            const prev_half_height = (prev.height * 0.5)
            const prev_center      = prev.y + prev_half_height - (prev.height * prev_anchor.y);

            const curr = objs[i];
            const curr_anchor = Layout.get_anchor_from(curr);
            curr.y = (prev_center + prev_half_height) + (curr.height *  curr_anchor.y) + gap;
        }
    }

    static stack_positions_h(gap, ...objs)
    {
        for(let i = 1; i < objs.length; ++i) {
            const prev = objs[i-1];
            const prev_left  = Layout.get_left_of(prev);
            const prev_width = prev.width;

            const curr = objs[i];
            const curr_left = prev_left + prev_width + gap;
            Layout.set_left_of(curr_left, curr);
        }
    }


    //
    // Align
    //
    static align_to_center_v(first, ...objs)
    {
        const first_anchor     = Layout.get_anchor_from(first);
        const first_half_width = first.width * 0.5;
        const first_center     = first.x - (first_anchor.x * first.width) + first_half_width;

        for(let i = 1; i < objs.length; ++i) {
            const curr = objs[i];
            const curr_anchor = Layout.get_anchor_from(curr);
            const x  = first_center + (curr_anchor.x * curr.width) - (curr.width * 0.5);
            curr.x = x;
        }
    }
}
