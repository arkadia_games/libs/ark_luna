//----------------------------------------------------------------------------//
//                                         888                                //
//                                         888                                //
//                                         888                                //
//                         8888b.  888d888 888  888                           //
//                            `88b 888P'   888 .88P                           //
//                        .d888888 888     888888K                            //
//                        888  888 888     888 `88b                           //
//                        `Y888888 888     888  888                           //
//                                                                            //
//                                                                            //
//  File      : app.mjs                                                       //
//  Project   : doom_fire                                                     //
//  Date      : 14 Dec, 21                                                    //
//  License   : GPLv3                                                         //
//  Author    : stdmatt <stdmatt@pixelwizards.io>                             //
//  Copyright : stdmatt - 2021                                                //
//                                                                            //
//  Description :                                                             //
//                                                                            //
//---------------------------------------------------------------------------~//

//----------------------------------------------------------------------------//
// Imports                                                                    //
//----------------------------------------------------------------------------//
//------------------------------------------------------------------------------
import { Scene_Manager } from "./scene_manager.mjs";
import { Layout        } from "./layout.mjs";
import { Tween_Group   } from "./tween.mjs";
import { Math_Utils    } from "./math_utils.mjs";
import { Log_Utils     } from "./log_utils.mjs";

//----------------------------------------------------------------------------//
// App                                                                        //
//----------------------------------------------------------------------------//
//------------------------------------------------------------------------------
export class App
{
    //------------------------------------------------------------------------//
    // Private Vars                                                           //
    //------------------------------------------------------------------------//
    //--------------------------------------------------------------------------
    static _config         = null;
    static _pixi_instance  = null;
    static _max_delta_time = 0;


    //------------------------------------------------------------------------//
    // Public Vars                                                            //
    //------------------------------------------------------------------------//
    //--------------------------------------------------------------------------
    static delta_time = 0;
    static total_time = 0;
    // Callbacks
    static pre_init = null;
    static pre_load = null;
    static init     = null;
    static loop     = null;
    static resize   = null;


    //------------------------------------------------------------------------//
    // Configuration                                                          //
    //------------------------------------------------------------------------//
    //--------------------------------------------------------------------------
    static set_config(config)
    {
        App._config = config;
    }

    //------------------------------------------------------------------------//
    // Public Functions                                                       //
    //------------------------------------------------------------------------//
    //--------------------------------------------------------------------------
    static async start()
    {
        if(App.pre_init) {
            App.pre_init();
        }

        App._setup_pixi();

        if(App.pre_load) {
            await App.pre_load();
        }

        if(App.init) {
            App.init();
        }

        // Setup Pixi callbacks.
        App._pixi_instance.ticker.add(() => { App._on_update_callback() } );
        // Setup the Window Callbacks.
        window.onresize = App._on_resize_callback;
        // Set the timers.
        App._max_delta_time = 1.0 / App._config.target_fps;
        // Create the PIXI particles.
        if((typeof _MCOW_HACK_PIXI_PARTICLES_INIT) == "function") {
            _MCOW_HACK_PIXI_PARTICLES_INIT(PIXI);
        }

        this._on_resize_callback(true);
    }

    //------------------------------------------------------------------------//
    // Helpers                                                                //
    //------------------------------------------------------------------------//
    //--------------------------------------------------------------------------
    static set_clear_color(color)
    {
        App._pixi_instance.renderer.backgroundColor = color;
    }

    //--------------------------------------------------------------------------
    static get_size()
    {
        const r = App._pixi_instance.renderer;
        return Math_Utils.make_size(r.width, r.height);
    }

    static update_size()
    {
        App._on_resize_callback();
    }


    //------------------------------------------------------------------------//
    // PIXI                                                                   //
    //------------------------------------------------------------------------//
    //--------------------------------------------------------------------------
    static get_pixi_instance()
    {
        return App._pixi_instance;
    }

    //--------------------------------------------------------------------------
    static _setup_pixi()
    {
        const element = document.documentElement;

        let width  = element.clientWidth;
        let height = element.clientHeight;
        if(App._config.design_size) {
            width  = App._config.design_size.width;
            height = App._config.design_size.height;
        }

        App._pixi_instance = new PIXI.Application({
            width  : width,
            height : height
        });

        const canvas    = App._pixi_instance.view;
        const append_to = (App._config.append_to) ? App._config.append_to : document.body;
        append_to.appendChild(canvas);
    }

    //------------------------------------------------------------------------//
    // Other                                                                  //
    //------------------------------------------------------------------------//
    //--------------------------------------------------------------------------
    static display_hello()
    {
        Log_Utils.log_always("Hello from luna!");
    }


    //------------------------------------------------------------------------//
    // Window Callback                                                        //
    //------------------------------------------------------------------------//
    //--------------------------------------------------------------------------
    static _on_update_callback()
    {
        // Cap the delta time.
        let dt = App._pixi_instance.ticker.deltaMS / 1000;
        if(dt > App._max_delta_time) {
            dt = App._max_delta_time;
        }

        // Update timers.
        App.delta_time  = dt;
        App.total_time += dt;

        // Call the game loop.
        if(App.loop) {
            App.loop(dt);
        }

        // Update the tweens.
        const tween_groups = Tween_Group.get_tagged_groups();
        for(let [_, group] of tween_groups) {
            group.update(dt);
        }

        // Update the scene.
        Scene_Manager.on_update(dt);
    }

    //--------------------------------------------------------------------------
    static _on_resize_callback(dont_bubble = false)
    {
        if(!App.resize) {
            return;
        }

        // Let the user decide the size that the renderer to be...
        const new_rect = App.resize();

        // Set the size on renderer...
        const renderer = App.get_pixi_instance().renderer;
        renderer.view.style.width  = new_rect.width - 0 + "px";
        renderer.view.style.height = new_rect.height + "px";

        // Inform the scenes about the resize...
        if(!dont_bubble) {
            Scene_Manager.on_resize();
        }
    }
};
