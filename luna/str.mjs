export class Str
{
    //--------------------------------------------------------------------------
    static to_int(value)
    {
        // @todo(stdmatt): Maybe debug is isn't valid???
        return parseInt(value);
    }

    //--------------------------------------------------------------------------
    static concat(...args)
    {
        let v = "";
        for(let i = 0; i < args.length; ++i) {
            v += args[i];
        }
        return v;
    }

    //--------------------------------------------------------------------------
    static format(fmt, ...args)
    {
        // @XXX(stdmatt): [Missing last char] - 14 Dec, 2021 at 03:46:07
        // The format is eating the last char of the fmt string.
        if(args.length == 0) {
            return fmt;
        }

        let final_str            = "";
        let index_tag_open       = 0;
        let index_tag_close      = 0;
        let index_to_copy        = 0;
        let positional_arg_index = 0;

        index_to_copy = index_tag_open;
        while(true)
        {
            // @todo(stdmatt); Check the cases when we have just one of the chars.

            // Find where in the string we have the {
            let index = fmt.indexOf("{", index_tag_open);
            if(index == -1) {
                break;
            }
            index_tag_open = index;

            // Find where in the string we have the }
            index = fmt.indexOf("}", index_tag_open);
            if(index == -1) {
                break;
            }
            index_tag_close = index;

            //
            final_str = final_str
                + fmt.substring(index_to_copy, index_tag_open)
                + args[positional_arg_index];

            index_to_copy  = index_tag_close + 1;
            index_tag_open = index_tag_close + 1;

            ++positional_arg_index;
        }

        return final_str;
    }
}
