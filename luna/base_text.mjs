
export class Base_TTFText
    extends PIXI.Text
{
    constructor(font_info, text, color)
    {
        super(text);
        this.font_info = font_info;
        this.style = new PIXI.TextStyle({
            fontSize: font_info.size,
            fontFamily: font_info.family
        });
    }
};

// Base_BMPText
//
//------------------------------------------------------------------------------
class Base_BMPText
    extends PIXI.BitmapText
{
    //--------------------------------------------------------------------------
    constructor(font_info)
    {
        const name = luna.Str.concat(size, "px", " ", family);
        super(str.toString(), {font: name});
        this.tint = fill;
    } // ctor

}; // class Base_BMPText
