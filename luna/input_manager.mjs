//----------------------------------------------------------------------------//
//                                         888                                //
//                                         888                                //
//                                         888                                //
//                         8888b.  888d888 888  888                           //
//                            `88b 888P'   888 .88P                           //
//                        .d888888 888     888888K                            //
//                        888  888 888     888 `88b                           //
//                        `Y888888 888     888  888                           //
//                                                                            //
//                                                                            //
//  File      : input_manager.mjs                                             //
//  Project   : berzerk                                                       //
//  Date      : 15 Dec, 21                                                    //
//  License   : GPLv3                                                         //
//  Author    : stdmatt <stdmatt@pixelwizards.io>                             //
//  Copyright : stdmatt - 2021                                                //
//                                                                            //
//  Description :                                                             //
//                                                                            //
//---------------------------------------------------------------------------~//

//----------------------------------------------------------------------------//
// Import                                                                     //
//----------------------------------------------------------------------------//
//------------------------------------------------------------------------------
import { App   } from "./app.mjs"
import { Utils } from "./utils.mjs";


//----------------------------------------------------------------------------//
// _Handler                                                                   //
//----------------------------------------------------------------------------//
class _Handler
{
    //--------------------------------------------------------------------------
    constructor()
    {
        this.handlers = [];

        this.handlers[Input_Manager.EVENT_ON_CLICK  ] = new Map();
        this.handlers[Input_Manager.EVENT_ON_PRESS  ] = new Map();
        this.handlers[Input_Manager.EVENT_ON_RELEASE] = new Map();
    }

    //--------------------------------------------------------------------------
    add_handler(event_type, handler_func)
    {
        const obj_id = Utils.get_luna_unique_object_id(handler_func);
        const map    = this.handlers[event_type];

        if(map.get(obj_id) != null) {
            Log_Utils.log_error("Already has a handler with this id", obj_id);
        }
        map.set(obj_id, handler_func);
    }

    //--------------------------------------------------------------------------
    fire_handlers(event_type, value) {
        const map = this.handlers[event_type];
        for (let [_, f] of map.entries()) {
            f(value);
        }
    }
}


//----------------------------------------------------------------------------//
// Mouse                                                                      //
//----------------------------------------------------------------------------//
//------------------------------------------------------------------------------
class Mouse
    extends _Handler
{
    // @notice(stdmatt): [PRIVATE CONSTRUCTOR] - 15 Dec, 2021 at 11:17:21
    // Access via Input_Manager.Mouse instead.
    constructor()
    {
        super();

        this.x = 0;
        this.y = 0;

        this.prev_x = 0;
        this.prev_y = 0;

        this.left_button   = false;
        this.right_button  = false;
        this.middle_button = false;

        this.wheel = { x: 0, y: 0 };
    }
}


//----------------------------------------------------------------------------//
// Keyboard                                                                   //
//----------------------------------------------------------------------------//
//------------------------------------------------------------------------------
class Keyboard
    extends _Handler
{
    // @notice(stdmatt): [PRIVATE CONSTRUCTOR] - 15 Dec, 2021 at 11:17:21
    // Access via Input_Manager.Mouse instead.
    constructor()
    {
        super();
    }
}


//----------------------------------------------------------------------------//
// Input_Manager                                                              //
//----------------------------------------------------------------------------//
//------------------------------------------------------------------------------
export class Input_Manager
{
    //------------------------------------------------------------------------//
    // Constants                                                              //
    //------------------------------------------------------------------------//
    //--------------------------------------------------------------------------
    static EVENT_ON_CLICK   = 0;
    static EVENT_ON_PRESS   = 1;
    static EVENT_ON_RELEASE = 2;


    //------------------------------------------------------------------------//
    // Vars                                                                   //
    //------------------------------------------------------------------------//
    //--------------------------------------------------------------------------
    //--------------------------------------------------------------------------
    static Keyboard         = null;
    static Mouse            = null;

    static is_mouse_enabled    = false;
    static is_keyboard_enabled = false;


    //------------------------------------------------------------------------//
    // LifeCycle                                                              //
    //------------------------------------------------------------------------//
    //--------------------------------------------------------------------------
    //--------------------------------------------------------------------------
    static init()
    {
        Input_Manager.Mouse    = new Mouse   ();
        Input_Manager.Keyboard = new Keyboard();
    }

    //------------------------------------------------------------------------//
    // Mouse                                                                  //
    //------------------------------------------------------------------------//
    //--------------------------------------------------------------------------
    //--------------------------------------------------------------------------
    static install_basic_mouse_handlers(html_element)
    {
        Input_Manager.is_mouse_enabled = true;
        html_element = (html_element || App.get_pixi_instance().view);

        //
        // Move
        html_element.addEventListener("mousemove", function(e) {
            const r = html_element.getBoundingClientRect();

            Input_Manager.Mouse.prev_x = Input_Manager.Mouse.x;
            Input_Manager.Mouse.prev_y = Input_Manager.Mouse.y;

            const x = (e.clientX - r.left) / (r.right  - r.left) * html_element.width;
            const y = (e.clientY - r.top ) / (r.bottom - r.top ) * html_element.height;

            Input_Manager.Mouse.x = x;
            Input_Manager.Mouse.y = y;
        }, false);

        //
        // Left Mouse Click
        html_element.addEventListener("click", (ev) => {
            Input_Manager.Mouse.left_button = true;
            Input_Manager.Mouse.fire_handlers(Input_Manager.EVENT_ON_CLICK, event);
        });

        //
        // Right Mouse Click
        html_element.addEventListener("contextmenu", (ev) => {
            ev.preventDefault();

            Input_Manager.Mouse.right_button = true;
            Input_Manager.Mouse.fire_handlers(Input_Manager.EVENT_ON_CLICK, ev);
        }, false);

        //
        // Mouse Down
        html_element.addEventListener("mousedown", (ev) => {
            // @todo(stdmatt): [CHECK WHICH BUTTON IS PRESSED] - 15 Dec, 2021 at 11:43:22
            if(ev) {
                Input_Manager.Mouse.left_button = true;
            } else if(ev) {
                Input_Manager.Mouse.middle_button = true;
            } else {
                Input_Manager.Mouse.right_button = true;
            }

            Input_Manager.Mouse.fire_handlers(Input_Manager.EVENT_ON_PRESS, ev);
        });

        //
        // Mouse Up
        html_element.addEventListener("mouseup", (ev) => {
            // @todo(stdmatt): [CHECK WHICH BUTTON IS PRESSED] - 15 Dec, 2021 at 11:43:22
            if(ev) {
                Input_Manager.Mouse.left_button = false;
            } else if(ev) {
                Input_Manager.Mouse.middle_button = false;
            } else {
                Input_Manager.Mouse.right_button = false;
            }

            Input_Manager.Mouse.fire_handlers(Input_Manager.EVENT_ON_RELEASE, ev);
         });

         //
         // Mouse Wheel
         html_element.addEventListener("wheel", (ev) => {
            Input_Manager.Mouse.wheel.x += ev.wheelDeltaX;
            Input_Manager.Mouse.wheel.y += ev.wheelDeltaY;
         });
    }

    //------------------------------------------------------------------------//
    // Keyboard                                                               //
    //------------------------------------------------------------------------//
    //--------------------------------------------------------------------------
    //--------------------------------------------------------------------------
    static install_basic_keyboard_handlers(html_element = window)
    {
        Input_Manager.is_keyboard_enabled = true;

        //
        // Keydown
        html_element.addEventListener("keydown", (ev) => {
            Input_Manager.Keyboard.fire_handlers(Input_Manager.EVENT_ON_PRESS, ev);
        });

        //
        // Keyup
        html_element.addEventListener("keyup", (ev) => {
            Input_Manager.Keyboard.fire_handlers(Input_Manager.EVENT_ON_RELEASE, ev);
        });

        //
        // Keypress
        html_element.addEventListener('keypress', (ev) =>  {
           Input_Manager.Keyboard.fire_handlers(Input_Manager.EVENT_ON_CLICK, ev);
        });
    }
};
